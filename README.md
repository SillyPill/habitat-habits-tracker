# habitat habits tracker

A RESTful Habits tracker API written using the Slim Framework ( PHP )

I made this project for the Web Technologies Lab in the 7th semester of
Engineering. This was developmed within a span of 12 hours, so one must expect
a few bugs here and there. 

I probably will not be maintaining this repository. However, if anyone wants to
develop this further for production, send me an email and I shall write some
nice documentation to refer to.

# How to run

1. Create a database called `habits` and source ```db.sql```
1. Run PHP server with ```public``` as root

# Misc

If you are looking for a good Habits tracking application,
[this](https://github.com/iSoron/uhabits) is what I use.


### Slim Framework 3 documentation

Use this skeleton application to quickly setup and start working on a new Slim Framework 3 application. This application uses the latest Slim 3 with the PHP-View template renderer. It also uses the Monolog logger.

This skeleton application was built for Composer. This makes setting up a new Slim Framework application quick and easy.

#### Install the Application

Run this command from the directory in which you want to install your new Slim Framework application.

    php composer.phar create-project slim/slim-skeleton [my-app-name]

Replace `[my-app-name]` with the desired directory name for your new application. You'll want to:

* Point your virtual host document root to your new application's `public/` directory.
* Ensure `logs/` is web writeable.

To run the application in development, you can run these commands 

	cd [my-app-name]
	php composer.phar start

Run this command in the application directory to run the test suite

	php composer.phar test

That's it! Now go build something cool.
